package br.gov.fazenda.nfeprocessa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class NfeProcessaApplication {

	public static void main(String[] args) {
		SpringApplication.run(NfeProcessaApplication.class, args);
	}

}
