package br.gov.fazenda.nfeprocessa.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Solicitação de nota fiscal eletrônica não encontrada")
public class NfeNotFoundException extends RuntimeException {
}
