package br.gov.fazenda.nfeprocessa.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Company {
    @JsonProperty("nome")
    private String name;

    @JsonProperty("capital_social")
    private Double sharedAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSharedAmount() {
        return sharedAmount;
    }

    public void setSharedAmount(Double sharedAmount) {
        this.sharedAmount = sharedAmount;
    }
}
