package br.gov.fazenda.nfeprocessa.clients;

import br.gov.fazenda.nfeprocessa.auth.OAuth2FeignConfiguration;
import br.gov.fazenda.nfeprocessa.dto.Company;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "company", url = "http://www.receitaws.com.br/v1/cnpj")
public interface CompanyClient {
    @GetMapping(value={"/{cnpj}"})
    Company getCompanyDetail(@PathVariable(value = "cnpj") String cnpj);
}

