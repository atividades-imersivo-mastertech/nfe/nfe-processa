package br.gov.fazenda.nfeprocessa.clients;

import br.gov.fazenda.nfeprocessa.auth.OAuth2FeignConfiguration;
import br.gov.fazenda.nfeprocessa.dto.NfeDetailRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "nfe", configuration = OAuth2FeignConfiguration.class)
public interface NfeClient {
    @PutMapping(value = "/attach/{id_nfeRequest}")
    void processNfe(@RequestBody() NfeDetailRequest nfeDetailRequest, @PathVariable(value = "id_nfeRequest") Long idNfeRequest);
}
