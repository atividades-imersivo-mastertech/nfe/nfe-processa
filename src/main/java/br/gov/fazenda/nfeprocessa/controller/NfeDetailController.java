package br.gov.fazenda.nfeprocessa.controller;

import br.gov.fazenda.messaging.model.NfeMessaging;
import br.gov.fazenda.nfeprocessa.service.NfeDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class NfeDetailController {
    @Autowired
    private NfeDetailService service;

    @KafkaListener(topics = "kaique-biro-1", groupId = "kaiquera")
    public void runNfeDetailProcess(@Payload NfeMessaging nfeMessaging) {
        service.processNfeDetail(nfeMessaging);
    }
}
