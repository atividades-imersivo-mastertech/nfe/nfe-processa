package br.gov.fazenda.nfeprocessa.service;

import org.springframework.stereotype.Service;

@Service
public class TaxesService {
    final static private Double IRRF_QUOTE = 0.015;
    final static private Double CSSL_QUOTE = 0.03;
    final static private Double PIS_COF_QUOTE = 0.0065;

    public Double getIRRFAmount(Double amount) {
        return amount * IRRF_QUOTE;
    }

    public Double getCSSLAmount(Double amount){
        return amount * CSSL_QUOTE;
    }

    public Double getPisCofQuote(Double amount) {
        return amount * PIS_COF_QUOTE;
    }
}
