package br.gov.fazenda.nfeprocessa.service;

import br.gov.fazenda.messaging.model.NfeGenericLog;
import br.gov.fazenda.messaging.model.NfeMessaging;
import br.gov.fazenda.nfeprocessa.clients.CompanyClient;
import br.gov.fazenda.nfeprocessa.clients.NfeClient;
import br.gov.fazenda.nfeprocessa.dto.Company;
import br.gov.fazenda.nfeprocessa.dto.NfeDetailRequest;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;

@Service
public class NfeDetailService {
    @Autowired
    private CompanyClient companyClient;

    @Autowired
    private NfeClient nfeClient;

    @Autowired
    private TaxesService taxesService;

    @Autowired
    private KafkaTemplate<String, NfeGenericLog> logSender;

    public void processNfeDetail(NfeMessaging nfeMessaging) {
        NfeDetailRequest nfeDetailRequest;
        if (isCnpj(nfeMessaging.getCpfCnpj())) {
            Company company = companyClient.getCompanyDetail(nfeMessaging.getCpfCnpj());

            if (company.getSharedAmount() >= 1000000.00) {
                nfeDetailRequest = createNfeDeatilForBig(nfeMessaging);
            } else {
                nfeDetailRequest = createNfeDetailForSimple(nfeMessaging);
            }
        } else {
            nfeDetailRequest = createNfeDetailForCpf(nfeMessaging);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            System.out.println(objectMapper.writeValueAsString(nfeDetailRequest));
        } catch (IOException e) {
            e.printStackTrace();
        }
        nfeClient.processNfe(nfeDetailRequest, nfeMessaging.getId());
        logSender.send("kaique-biro-2", "kaiqueraLogger", new NfeGenericLog(new Date(), "Processamento", "Foi processada a nota requisitada por " + nfeMessaging.getCpfCnpj()));
    }

    private Boolean isCnpj(String cpfCnpj) {
        return cpfCnpj.length() > 11;
    }

    private NfeDetailRequest createNfeDetailForCpf(NfeMessaging nfeMessaging) {
        NfeDetailRequest nfeDetailRequest = new NfeDetailRequest();

        nfeDetailRequest.setInitialAmount(nfeMessaging.getAmount());
        nfeDetailRequest.setIRRFAmount(0.0);
        nfeDetailRequest.setCSSLAmount(0.0);
        nfeDetailRequest.setCofinsAmount(0.0);
        nfeDetailRequest.setFinalAmount(nfeMessaging.getAmount());

        return nfeDetailRequest;
    }

    private NfeDetailRequest createNfeDetailForSimple(NfeMessaging nfeMessaging) {
        NfeDetailRequest nfeDetailRequest = new NfeDetailRequest();

        nfeDetailRequest.setInitialAmount(nfeMessaging.getAmount());
        nfeDetailRequest.setIRRFAmount(taxesService.getIRRFAmount(nfeDetailRequest.getInitialAmount()));
        nfeDetailRequest.setCSSLAmount(0.0);
        nfeDetailRequest.setCofinsAmount(0.0);
        nfeDetailRequest.setFinalAmount(calcFinalAmount(nfeDetailRequest));

        return nfeDetailRequest;
    }

    private NfeDetailRequest createNfeDeatilForBig(NfeMessaging nfeMessaging) {
        NfeDetailRequest nfeDetailRequest = new NfeDetailRequest();

        nfeDetailRequest.setInitialAmount(nfeMessaging.getAmount());
        nfeDetailRequest.setIRRFAmount(taxesService.getIRRFAmount(nfeDetailRequest.getInitialAmount()));
        nfeDetailRequest.setCSSLAmount(taxesService.getCSSLAmount(nfeDetailRequest.getInitialAmount()));
        nfeDetailRequest.setCofinsAmount(taxesService.getPisCofQuote(nfeDetailRequest.getInitialAmount()));
        nfeDetailRequest.setFinalAmount(calcFinalAmount(nfeDetailRequest));

        return nfeDetailRequest;
    }

    private Double calcFinalAmount(NfeDetailRequest nfeDetailRequest) {
        return nfeDetailRequest.getInitialAmount() - nfeDetailRequest.getCSSLAmount() - nfeDetailRequest.getIRRFAmount() - nfeDetailRequest.getCofinsAmount();
    }
}
